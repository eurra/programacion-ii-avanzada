package sem1_2023.poo1;

import java.util.Locale;
import java.util.Scanner;

public class App3 {
    public static void main(String[] args) {
        CuentaCorriente[] cuentas = new CuentaCorriente[3];

        for(int i = 0; i < cuentas.length; i++)
            System.out.println("valor de cuenta " + i + " = " + cuentas[i]);

        Scanner lector = new Scanner(System.in);

        for(int i = 0; i < cuentas.length; i++) {
            System.out.println("Ingrese nombre " + i + ":");
            String nt = lector.next();

            System.out.println("Ingrese rut " + i + ":");
            String r = lector.next();

            System.out.println("Ingrese saldo inicial " + i + ":");
            int si = lector.nextInt();

            CuentaCorriente nueva = new CuentaCorriente(nt, r, si);
            cuentas[i] = nueva;
        }

        //cuentas[2] = null;

        for(int i = 0; i < cuentas.length; i++) {
            if(cuentas[i] == null)
                System.out.println("Este dato es nulo!");
            else
                System.out.println("valor del indice " + i + " = " + cuentas[i]);
        }
    }
}
