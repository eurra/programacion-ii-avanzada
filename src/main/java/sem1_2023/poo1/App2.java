package sem1_2023.poo1;

import java.util.Locale;
import java.util.Scanner;

public class App2 {
    public static void main(String[] args) {
        String[] numeros = new String[5];

        for(int i = 0; i < numeros.length; i++)
            System.out.println("valor del indice " + i + " = " + numeros[i]);

        Scanner lector = new Scanner(System.in);

        for(int i = 0; i < numeros.length; i++) {
            System.out.println("Ingrese nombre " + i + ":");
            numeros[i] = lector.next();
        }

        numeros[2] = null;

        for(int i = 0; i < numeros.length; i++) {
            if(numeros[i] == null)
                System.out.println("Este dato es nulo!");
            else
                System.out.println("valor del indice " + i + " = " + numeros[i].toUpperCase(Locale.ROOT));
        }
    }
}
