package sem1_2023.poo1;

public class Aplicacion {
    public static void main(String[] args) {
        CuentaCorriente cc = new CuentaCorriente("Victor Ovando", "18818998-9", 0);
        CuentaCorriente cc2 = new CuentaCorriente("Enrique Urra", "15070015-9", -100);

        imprimirDatosCuenta(cc);
        imprimirDatosCuenta(cc2);

        cc.depositar(1000000);
        //cc2.depositar(100);

        imprimirDatosCuenta(cc);
        imprimirDatosCuenta(cc2);

        cc.transferir(cc2, 900000);

        imprimirDatosCuenta(cc);
        imprimirDatosCuenta(cc2);
    }

    public static void imprimirDatosCuenta(CuentaCorriente c) {
        System.out.println("Saldo cuenta de " + c.obtenerNombreTitular() + ": " + c.obtenerSaldo());
        System.out.println("Nro transacciones cuenta de " + c.obtenerNombreTitular() + ": " + c.obtenerNroTransacciones());
    }
}
