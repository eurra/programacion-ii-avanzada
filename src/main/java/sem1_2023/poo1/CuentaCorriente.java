package sem1_2023.poo1;

public class CuentaCorriente {
    private int saldo;
    private String nombreTitular;
    private String rut;
    private int nroGiros;
    private int nroDepositos;

    public CuentaCorriente(String nt, String r, int si) {
        nombreTitular = nt;
        rut = r;
        nroDepositos = 0;
        nroGiros = 0;

        if(si > 0)
            saldo = si;
        else
            saldo = 0;
    }

    public int obtenerSaldo() {
        return saldo;
    }

    public String obtenerNombreTitular() {
        return nombreTitular;
    }

    public int depositar(int montoADepositar) {
        if(montoADepositar > 0) {
            saldo = saldo + montoADepositar;
            nroDepositos = nroDepositos + 1;
        }

        return saldo;
    }

    public int girar(int montoAGirar) {
        if(montoAGirar > 0 && montoAGirar <= saldo) {
            saldo = saldo - montoAGirar;
            nroGiros = nroGiros + 1;
        }

        return saldo;
    }

    public int transferir(CuentaCorriente destino, int monto) {
        girar(monto);
        destino.depositar(monto);

        return saldo;
    }

    @Override
    public String toString() {
        return "CuentaCorriente{" +
                "saldo=" + saldo +
                ", nombreTitular='" + nombreTitular + '\'' +
                ", rut='" + rut + '\'' +
                ", nroGiros=" + nroGiros +
                ", nroDepositos=" + nroDepositos +
                '}';
    }

    public int obtenerNroTransacciones() {
        return nroGiros + nroDepositos;
    }
}
