package sem1_2023.refs.disenio2;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        Vehiculo[] vehs = new Vehiculo[100];
        boolean salir = false;

        do {
            System.out.println("Seleccione opción:");
            System.out.println("1 - Agregar nuevo vehículo");
            System.out.println("2 - Optimizar espacios");
            System.out.println("3 - Determinar ganancia total");
            System.out.println("4 - Determinar porcentaje de ocupación");
            System.out.println("5 - Salir");

            int opcion = lector.nextInt();

            switch(opcion) {
                case 1: {
                    // Pedimos primero la cantidad de espacios, ya que si no hay, no tiene
                    // sentido pedir otras cosas
                    System.out.println("Ingrese cantidad de unidades de espacio que ocupa vehículo:");
                    int espacios = lector.nextInt();
                    int indEspacioRequerido = buscarEspacioLibre(vehs, espacios);

                    if(indEspacioRequerido == -1) {
                        System.out.println("No se encontró espacio para el vehículo en el estacionamiento");
                    }
                    else {
                        // Vemos si existe vehículo en el arreglo con la misma patente
                        System.out.println("Ingrese patente:");
                        String patente = lector.next();
                        int posPatente = buscarPatente(vehs, patente);

                        if(posPatente != -1) {
                            System.out.println("Ya existe un vehículo con dicha patente en el estacionamiento");
                        }
                        else {
                            // Ya acá, podemos agregar con confianza el vehículo
                            System.out.println("Ingresar cantidad de días:");
                            int dias = lector.nextInt();

                            // Creamos el vehículo nuevo y lo agregamos en desde el índice antes
                            // encontrado, la cantidad de veces que corresponda a su espacio (para
                            // que los ocupe)
                            Vehiculo nuevo = new Vehiculo(patente, espacios, dias);

                            for(int i = 0; i < espacios; i++)
                                vehs[indEspacioRequerido + i] = nuevo;

                            System.out.println("Vehículo agregado!");
                        }
                    }

                    break;
                }
                case 2: {
                    System.out.println("No implementado aún.");
                    break;
                }
                case 3: {
                    /**
                     * Notar que en la clase Vehículo implementamos la lógica de cálculo del
                     * costo de cada vehículo (método {@link Vehiculo#calcularCosto()}, incluída
                     * la aplicación de los descuentos.
                     *
                     * Luego, acá solo nos encargamos de totalizar.
                     */
                    double total = 0;
                    int i = 0;

                    // Usaremos un while, ya que un vehículo podría ocupar multiples espacios
                    // contiguos, y debemos preocuparnos de no sumar duplicado
                    while(i < vehs.length) {
                        // Importante: revisar que no sea null!
                        if(vehs[i] != null) {
                            total += vehs[i].calcularCosto();

                            // Como encontramos un espacio ocupado, debemos asegurarnos de que
                            // no sumemos duplicado el costo del mismo vehículo. Para ello,
                            // avanzaremos el contador i hasta encontrar un valor distinto a la
                            // referencia del vehículo actual.
                            int actual = i;

                            while(i < vehs.length && vehs[actual] == vehs[i])
                                i++;
                        }
                        else {
                            i++;
                        }
                    }

                    System.out.println("La ganancia total del estacionamiento en el estado actual es: " + total);
                    break;
                }
                case 4: {
                    System.out.println("No implementado aún.");
                    break;
                }
                case 5: {
                    salir = true;
                    break;
                }
                default: {
                    System.out.println("Opción inválida");
                    break;
                }
            }
        }
        while(!salir); // igual a salir == false

        System.out.println("Adios!");
    }

    // Función para evaluar espacios disponibles en arreglo.
    // Retornará el índice inicial en el arreglo donde hay espacio disponible, o -1 si no hay.
    // Nota: en diseño 2, esto se moverá a la clase Estacionamiento.
    private static int buscarEspacioLibre(Vehiculo[] arr, int cantEspacios) {
        // Acá se irán contando los espacios libres contiguos que encontremos
        // Cuando el valor es -1, significará que estamos partiendo una nueva cuenta
        int cuentaContigua = -1;
        // Acá se guardará el índice donde parten los espacios libres contiguos que llevamos
        int espacioLibre = -1;

        for(int i = 0; i < arr.length; i++) {
            // ¿Estamos partiendo una cuenta nueva? entonces...
            if(cuentaContigua == -1) {
                cuentaContigua = 0; // reiniciamos contador de espacios contiguos
                espacioLibre = i; // guardamos el primer índice consecutivo
            }

            if(arr[i] == null) { // espacio libre
                cuentaContigua++;

                // Si encontramos la cantidad de espacios contiguos disponibles que necesitamos,
                // entonces retornamos el índice encontrado.
                if(cuentaContigua == cantEspacios) {
                    return espacioLibre;
                }
            }
            // En caso contrario, reseteamos el contador de espacios contiguos, para que en
            // la próxima iteración empecemos a contar espacios de nuevo
            else {
                cuentaContigua = -1;
            }
        }

        // Si llegamos acá, es que recorrimos todo el arreglo y no encontramos el espacio requerido
        return -1;
    }

    // Función para buscar si existe la patente en el arreglo.
    // Retorna el índice en donde se encuentra, o -1 si no está.
    private static int buscarPatente(Vehiculo[] arr, String patente) {
        for(int i = 0; i < arr.length; i++) {
            // Importante: revisar que no sea null!
            if(arr[i] != null && arr[i].getPatente().equals(patente))
                return i; // Encontrado en posición i!
        }

        // Si llegamos acá, revisamos todo el arreglo y no encontramos la patente
        return -1;
    }
}
