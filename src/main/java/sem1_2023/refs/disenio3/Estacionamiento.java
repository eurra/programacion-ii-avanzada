package sem1_2023.refs.disenio3;

/**
 * A esta clase se moverá buena parte de la lógica de trabajo con el estacionamiento que estaba antes
 * en App, incluyendo el arreglo de Vehículos. De este modo, estos datos quedan encapsulados y App no
 * se mete con ellos.
 *
 * Notar que esta clase no manejará ningún tipo de interacción con el usuario, ya sea lectura
 * por teclado y mostrar resultados por pantalla, la idea es que eso quede solo en App.
 */
public class Estacionamiento {
    private Vehiculo[] vehs;

    // Si bien no se pide tamaño, igual parametrizamos acá para futuro uso.
    // En App se asigna 100.
    public Estacionamiento(int tam) {
        this.vehs = new Vehiculo[tam];
    }

    /**
     * Método para implementar la funcionalidad i) del enunciado. Esté método se trae el código
     * que estaba en App del diseño 2. Luego, en App solo se invocará y utilizará su resultado.
     *
     * Notar que recibe solo los datos necesarios para validar y crear un nuevo vehículo.
     * Debe ser public, para que sea accesible por App.
     *
     * Por ahora, sólo retornará true / false (boolean) según el resultado... ¿Cómo se podría
     * ajustar para identificar si hay un error específico?
     */
    public boolean agregarNuevoVehículo(String patente, int espacios, int dias){
        int indEspacioRequerido = buscarEspacioLibre(espacios);

        if(indEspacioRequerido == -1) {
            return false;
        }
        else {
            int posPatente = buscarPatente(patente);

            if(posPatente != -1) {
                return false;
            }
            else {
                Vehiculo nuevo = new Vehiculo(patente, espacios, dias);

                for(int i = 0; i < espacios; i++)
                    vehs[indEspacioRequerido + i] = nuevo;

                return true;
            }
        }
    }

    /**
     * Método para implementar la funcionalidad ii) del enunciado, en la misma forma de diseño que
     * los anteriores.
     */
    public double calcularGananciaEstacionamiento() {
        double total = 0;
        int i = 0;

        while(i < vehs.length) {
            if(vehs[i] != null) {
                total += vehs[i].calcularCosto();
                int actual = i;

                while(i < vehs.length && vehs[actual] == vehs[i])
                    i++;
            }
            else {
                i++;
            }
        }

        return total;
    }

    // Modificación de método con mismo nombre en App del diseño 2.
    // Notar que ya no requiere recibir el arreglo como parámetro, ya que es un atributo de
    // la clase.
    private int buscarEspacioLibre(int cantEspacios) {
        int cuentaContigua = -1;
        int espacioLibre = -1;

        for(int i = 0; i < vehs.length; i++) {
            if(cuentaContigua == -1) {
                cuentaContigua = 0;
                espacioLibre = i;
            }

            if(vehs[i] == null) {
                cuentaContigua++;

                if(cuentaContigua == cantEspacios) {
                    return espacioLibre;
                }
            }
            else {
                cuentaContigua = -1;
            }
        }

        return -1;
    }

    // Modificación de método con mismo nombre en App del diseño 2.
    // Notar que ya no requiere recibir el arreglo como parámetro, ya que es un atributo de
    // la clase.
    private int buscarPatente(String patente) {
        for(int i = 0; i < vehs.length; i++) {
            if(vehs[i] != null && vehs[i].getPatente().equals(patente))
                return i;
        }

        return -1;
    }
}
