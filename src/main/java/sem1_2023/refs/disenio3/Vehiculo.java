package sem1_2023.refs.disenio3;
public class Vehiculo {
    private String patente;
    private int espacios;
    private int dias;

    public Vehiculo(String patente, int espacios, int dias) {
        this.patente = patente;
        this.espacios = espacios;
        this.dias = dias;
    }

    public double calcularCosto() {
        double costoBase = espacios * dias * 100;

        if(dias >= 5 && dias <= 10)
            costoBase *= 0.9;
        else if(dias > 10 && dias <= 25)
            costoBase *= 0.7;
        else if(dias > 25)
            costoBase *= 0.6;

        return costoBase;
    }

    public int getDias() {
        return dias;
    }

    public int getEspacios() {
        return espacios;
    }

    public String getPatente() {
        return patente;
    }
}
