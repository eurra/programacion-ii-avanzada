package sem1_2023.refs.disenio3;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        boolean salir = false;

        // En vez de tener el arreglo como en el diseño 2, acá usamos una referencia a un objeto de
        // tipo Estacionamiento, y trabajamos a través de sus métodos
        Estacionamiento estacionamiento = new Estacionamiento(100);

        do {
            System.out.println("Seleccione opción:");
            System.out.println("1 - Agregar nuevo vehículo");
            System.out.println("2 - Optimizar espacios");
            System.out.println("3 - Determinar ganancia total");
            System.out.println("4 - Determinar porcentaje de ocupación");
            System.out.println("5 - Salir");

            int opcion = lector.nextInt();

            switch(opcion) {
                case 1: {
                    System.out.println("Ingrese cantidad de unidades de espacio que ocupa vehículo:");
                    int espacios = lector.nextInt();
                    System.out.println("Ingrese patente:");
                    String patente = lector.next();
                    System.out.println("Ingresar cantidad de días:");
                    int dias = lector.nextInt();

                    // Acá simplemente usamos el método de la clase Estacionamiento, que encapsula
                    // toda la lógica de validación
                    if(estacionamiento.agregarNuevoVehículo(patente, espacios, dias))
                        System.out.println("Vehículo agregado correctamente.");
                    else
                        System.out.println("No se pudo agregar. Revise si hay espacio suficiente en el estacionamiento, o si patente no está repetida.");

                    break;
                }
                case 2: {
                    System.out.println("No implementado aún.");
                    break;
                }
                case 3: {
                    double total = estacionamiento.calcularGananciaEstacionamiento();
                    System.out.println("La ganancia total del estacionamiento en el estado actual es: " + total);
                    break;
                }
                case 4: {
                    System.out.println("No implementado aún.");
                    break;
                }
                case 5: {
                    salir = true;
                    break;
                }
                default: {
                    System.out.println("Opción inválida");
                    break;
                }
            }
        }
        while(!salir);

        System.out.println("Adios!");
    }
}
