package sem1_2023.colls.encomiendas;

public class Paquete {
    private int id;
    private String nombreTitular;
    private double peso;

    public Paquete(int id, String nombreTitular, double peso) {
        this.id = id;
        this.nombreTitular = nombreTitular;
        this.peso = peso;
    }

    public int getId() {
        return id;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public double calcularPrecio(double costoKg, double factorCentro) {
        double precio = this.peso * costoKg;

        if(factorCentro <= 0.1)
            precio *= 1.5;
        else if(factorCentro <= 0.3)
            precio *= 1.25;
        else if(this.peso > 10.0)
            precio *= 1.05;

        return precio;
    }
}
