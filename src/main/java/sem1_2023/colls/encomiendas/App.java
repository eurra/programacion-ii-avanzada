package sem1_2023.colls.encomiendas;

public class App {
    public static void main(String[] args) {
        CentroDistribucion centro = new CentroDistribucion(100);
        centro.setCostoKg(200);

        System.out.println(centro.agregarPaquete(1, "Enrique", 4.5)); // true
        System.out.println(centro.agregarPaquete(2, "Pepe", 2.0)); // true
        System.out.println(centro.agregarPaquete(1, "Hola", 5.0)); // false
        System.out.println(centro.agregarPaquete(3, "Test", 3.0)); // true

        System.out.println(centro.distribuirPaquete(2)); // 600
        System.out.println(centro.distribuirPaquete(2)); // -1.0
    }
}
