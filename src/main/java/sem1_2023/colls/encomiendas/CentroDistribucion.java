package sem1_2023.colls.encomiendas;

public class CentroDistribucion {
    private Paquete[] paquetes;
    private double costoKg;
    private double gananciasTotales;
    private int cantPaquetes;

    public CentroDistribucion(int maxPaquetes) {
        paquetes = new Paquete[maxPaquetes];
        gananciasTotales = 0.0;
    }

    public double getGananciasTotales() {
        return this.gananciasTotales;
    }

    public void setCostoKg(double costoKg) {
        this.costoKg = costoKg;
    }

    public boolean agregarPaquete(int id, String nombreTitular, double peso) {
        int pos = obtenerPosicionPaquete(id);

        if(pos != -1)
            return false; // paquete existía

        if(cantPaquetes == paquetes.length)
            return false; // No hay capacidad

        Paquete nuevo = new Paquete(id, nombreTitular, peso);
        paquetes[obtenerPosicionLibre()] = nuevo;
        cantPaquetes++;

        return true;
    }

    public double distribuirPaquete(int id) {
        int pos = obtenerPosicionPaquete(id);

        if(pos == -1)
            return -1;

        Paquete p = paquetes[pos];
        double valor = p.calcularPrecio(costoKg, (double)cantPaquetes / paquetes.length );
        gananciasTotales += valor;
        paquetes[pos] = null; // Distribuido

        return valor;
    }

    private int obtenerPosicionPaquete(int id) {
        for(int i = 0; i < paquetes.length; i++) {
            if(paquetes[i] != null && paquetes[i].getId() == id)
                return i;
        }

        return -1;
    }

    private int obtenerPosicionLibre() {
        for(int i = 0; i < paquetes.length; i++) {
            if(paquetes[i] == null)
                return i;
        }

        return -1;
    }
}
