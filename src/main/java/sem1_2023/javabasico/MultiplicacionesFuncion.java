package sem1_2023.javabasico;
public class MultiplicacionesFuncion {
    public static void main(String[] args) {
        System.out.println("Tablas de multiplicar");
        System.out.println();

        int tablaMax = 12;

        for(int i = 1; i <= tablaMax; i++) {
            mostrarTabla(i);
            System.out.println();
        }
    }

    public static void mostrarTabla(int tabla) {
        System.out.println("Tabla del " + tabla + ":");

        for (int j = 1; j <= 12; j++)
            System.out.println(tabla + " x " + j + " = " + (tabla * j));
    }
}