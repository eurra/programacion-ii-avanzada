package sem1_2023.javabasico;
public class Multiplicaciones {
    public static void main(String[] args) {
        System.out.println("Tablas de multiplicar");
        System.out.println();

        int tablaMax = 12;

        for(int i = 1; i <= tablaMax; i++) {
            System.out.println("Tabla del " + i);

            for (int j = 1; j <= 12; j++)
                System.out.println(i + " x " + j + " = " + (i * j));

            System.out.println();
        }
    }
}