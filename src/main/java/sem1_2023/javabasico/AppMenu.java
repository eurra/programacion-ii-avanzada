package sem1_2023.javabasico;

import java.util.Scanner;

public class AppMenu {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int opcion;
        boolean salir = false;

        do {
            System.out.println("Seleccione una de las siguientes opciones:");
            System.out.println("1- Sumar dos números.");
            System.out.println("2- Restar dos números.");
            System.out.println("3- ????");
            System.out.println("4- Salir.");

            opcion = lector.nextInt();

            switch(opcion) {
                case 1: {
                    System.out.println("Ingrese número 1:");
                    int num1 = lector.nextInt();

                    System.out.println("Ingrese número 2:");
                    int num2 = lector.nextInt();

                    int resultado = suma(num1, num2);
                    System.out.println("El resultado de la suma es: " + resultado);

                    break;
                }
                case 2: {
                    System.out.println("Ingrese número 1:");
                    int num1 = lector.nextInt();

                    System.out.println("Ingrese número 2:");
                    int num2 = lector.nextInt();

                    int resultado = resta(num1, num2);
                    System.out.println("El resultado de la suma es: " + resultado);

                    break;
                }
                case 3: {
                    System.out.println("No implementado");
                    break;
                }
                case 4: {
                    salir = true;
                    break;
                }
                default: {
                    System.out.println("Opción no válida");
                    break;
                }
            }

        }
        while(salir == false);

        System.out.println("Muchas gracias por su preferencia.");
    }

    public static int suma(int num1, int num2) {
        return num1 + num2;
    }

    public static int resta(int num1, int num2) {
        return num1 - num2;
    }
}
