package sem1_2023.javabasico;

import java.util.Scanner;

public class FibonacciRecursivo {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int max = -1;

        while(max < 2) {
            System.out.println("Ingrese máximo índice de la secuencia (mayor o igual a 2):");
            max = lector.nextInt();

            if(max < 2)
                System.out.println("Intente nuevamente.");
        }

        int prev = 0; int actual = 1;
        System.out.println("Valor 0 = " + prev);
        System.out.println("Valor 1 = " + actual);

        calculoSig(prev, actual, 2, max);
    }

    public static void calculoSig(int prev, int actual, int indActual, int max) {
        int nuevo = prev + actual;
        System.out.println("Valor " + indActual + " = " + nuevo);

        if(indActual + 1 >= max)
            return;

        calculoSig(actual, nuevo, indActual + 1, max);
    }
}