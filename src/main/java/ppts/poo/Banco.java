package ppts.poo;

import java.util.Scanner;

public class Banco {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        
        System.out.println("Ingrese cantidad de cuentas del banco");
        CuentaCorriente[] cuentas = new CuentaCorriente[lector.nextInt()];
        
        boolean continuar = true;
        
        do {
            System.out.println("Seleccione opción:");
            System.out.println("1 - Agregar nueva cuenta");
            System.out.println("2 - Depositar en una cuenta");
            System.out.println("3 - Calcular promedio de saldos");
            
            int opc = lector.nextInt();
            
            switch(opc) {
                case 1: {
                    System.out.println("Ingrese rut de nueva cuenta");
                    String nuevoRut = lector.next();
                    int posLibre = -1;
                    boolean existe = false;
                    
                    for(int i = 0; i < cuentas.length; i++) {
                        if(cuentas[i] == null)
                            posLibre = i;
                        else if(cuentas[i].getRut().equals(nuevoRut))
                            existe = true;
                    }
                    
                    if(existe) {
                        System.out.println("Rut ya existe en el banco");
                    }
                    else if(posLibre == -1) {
                        System.out.println("No hay espacio para agregar la cuenta");
                    }
                    else {
                        cuentas[posLibre] = new CuentaCorriente(nuevoRut);
                        System.out.println("Cuenta agregada exitosamente!");
                    }
                    
                    break;
                }
                case 2: {
                    System.out.println("Ingrese rut de cuenta donde se depositará");
                    String rutDestino = lector.next();
                    
                    boolean existe = false;
                    
                    for(int i = 0; i < cuentas.length; i++) {
                        if(cuentas[i] != null && cuentas[i].getRut().equals(rutDestino)) {
                            existe = true;
                            
                            System.out.println("Ingrese cantidad a depositar");
                            int cant = lector.nextInt();
                            
                            cuentas[i].depositar(cant);
                            System.out.println("Depósito exitoso!");
                        }
                    }
                    
                    if(!existe)
                        System.out.println("No se encontró la cuenta ingresada");
                    
                    break;
                }
                case 3: {
                    double suma = 0.0;
                    int cantCuentas = 0;
                    
                    for(int i = 0; i < cuentas.length; i++) {
                        if(cuentas[i] != null) {
                            cantCuentas++;
                            suma += cuentas[i].getSaldo();
                        }
                    }
                    
                    if(cantCuentas == 0)
                        System.out.println("No se encontraron cuentas ingresadas en el banco");
                    else
                        System.out.println("El promedio de los saldos de las cuentas es " + (suma / cantCuentas));
                        
                    break;
                }
            }
        }
        while(continuar);
    }
}
