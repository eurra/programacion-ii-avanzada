
package ppts.poo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class ObrerosConstru {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        ArrayList<Obrero> obreros = new ArrayList<>();
        HashMap<Integer, Obrero> asignaciones = new HashMap<>();
        boolean continuar = true;
        
        do {
            System.out.println("Seleccione opción:");
            System.out.println("1 - Agregar nuevo obrero.");
            System.out.println("2 - Asignar trabajo a obrero.");
            System.out.println("3 - Finalizar trabajo.");
            System.out.println("4 - Buscar obrero según trabajo asignado.");
            System.out.println("5 - Calcular ratio de ociosidad.");
            System.out.println("6 - Salir.");
            
            int opc = lector.nextInt();
            
            switch(opc) {
                case 1: {
                    System.out.println("Ingrese RUT del nuevo obrero");
                    String nuevoRut = lector.next();
                    boolean encontrado = false;
                    
                    for(int i = 0; i < obreros.size() && !encontrado; i++) {
                        if(obreros.get(i).getRut().equals(nuevoRut))
                            encontrado = true;
                    }
                    
                    if(encontrado) {
                        System.out.println("Obrero ya existe");
                    }
                    else {
                        System.out.println("Ingrese edad del obrero");
                        int edad = lector.nextInt();
                        obreros.add(new Obrero(nuevoRut, edad));
                        
                        System.out.println("Obrero agregado exitosamente!");
                    }
                    
                    break;
                }
                case 2: {
                    System.out.println("Ingrese ID de trabajo a asignar");
                    int idTrabajo = lector.nextInt();
                    
                    if(asignaciones.containsKey(idTrabajo)) {
                        System.out.println("Trabajo ya está asignado");
                    }
                    else {
                        System.out.println("Ingrese RUT del obrero a asignar");
                        String rutAsignar = lector.next();
                        Obrero asignar = null;
                        
                        for(int i = 0; i < obreros.size() && asignar == null; i++) {
                            if(obreros.get(i).getRut().equals(rutAsignar))
                                asignar = obreros.get(i);
                        }
                        
                        if(asignar == null) {
                            System.out.println("Obrero no existe");
                        }
                        else {
                            asignaciones.put(idTrabajo, asignar);
                            System.out.println("Trabajo asignado exitosamente!");
                        }                        
                    }
                    
                    break;
                }
                case 3: {
                    System.out.println("Ingrese ID de trabajo a asignar");
                    int idTrabajo = lector.nextInt();
                    
                    if(!asignaciones.containsKey(idTrabajo)) {
                        System.out.println("Trabajo no está asignado");
                    }
                    else {
                        asignaciones.remove(idTrabajo);
                        System.out.println("Trabajo eliminado!");
                    }
                    
                    break;
                }
                case 4: {
                    System.out.println("Ingrese ID de trabajo a asignar");
                    int idTrabajo = lector.nextInt();
                    
                    if(!asignaciones.containsKey(idTrabajo)) {
                        System.out.println("Trabajo no está asignado");
                    }
                    else {
                        Obrero ob = asignaciones.get(idTrabajo);
                        System.out.println("Rut del obrero asignado al trabajo " + idTrabajo + ": " + ob.getRut());
                    }
                    
                    break;
                }
                case 5: {
                    if(obreros.isEmpty()) {
                        System.out.println("No hay obreros que evaluar");
                    }
                    else {
                        int cantObrerosNoAsignados = obreros.size() - asignaciones.size();
                        System.out.println("El ratio de ociosidad es " + ((double)cantObrerosNoAsignados / obreros.size()));
                    }
                    break;
                }
                case 6: {
                    continuar = false;
                }
            }
        }
        while(continuar);
    }
}
