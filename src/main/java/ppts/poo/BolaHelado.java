
package ppts.poo;

public class BolaHelado {
    private String color;
    private String sabor;
 
    public BolaHelado(String c, 
                      String s) {
        color = c;
        sabor = s;
    }

    public String comer() {
        return "Delicioso helado de " + sabor + "!";
    }
}
