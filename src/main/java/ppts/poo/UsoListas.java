
package ppts.poo;

import java.util.ArrayList;

public class UsoListas {
    public static void main(String[] args) {
        // Crear lista
        ArrayList<CuentaCorriente> cuentas = new ArrayList<>();
        
        // Agregar elementos        
        cuentas.add(new CuentaCorriente("rut1"));
        
        CuentaCorriente c = new CuentaCorriente("rut2");
        cuentas.add(c);
        
        cuentas.add(new CuentaCorriente("rut3"));
        cuentas.add(new CuentaCorriente("rut4"));
        
        // Obtener elemento
        System.out.println(cuentas.get(0).getSaldo());
        
        // Iterar elementos
        for(int i = 0; i < cuentas.size(); i++)
            System.out.println("Rut de cuenta: " + cuentas.get(i).getRut());
        
        // Eliminar elementos
        cuentas.remove(2);
        cuentas.remove(c);
        
        // Obtener cantidad de elementos
        System.out.println("Tamaño: " + cuentas.size());
        
        // Reemplazar elementos
        cuentas.set(0, new CuentaCorriente("rut5"));
        
        for(int i = 0; i < cuentas.size(); i++)
            System.out.println("Rut de cuenta: " + cuentas.get(i).getRut());
                
        // Borrar todos los elementos
        cuentas.clear();
    }
}
