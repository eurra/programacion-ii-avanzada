
package ppts.poo;

public class CuentaCorriente {
    private String rut;
    private int saldo;
    private int numTransacciones;
    
    public CuentaCorriente(String rut) {
        this.rut = rut;
        saldo = 0;
        numTransacciones = 0;
    }

    public int getSaldo() {
        return saldo;
    }
    
    public int getNumeroTransacciones() {
        return numTransacciones;
    }

    public String getRut() {
        return rut;
    }
    
    public void depositar(int monto) {
        saldo += monto;
        numTransacciones++;
    }
    
    public boolean girar(int monto) {
        if(saldo - monto >= 0) {
            saldo -= monto;
            numTransacciones++;            
            return true;
        }
        
        return false;
    }
}
