
package ppts.poo;

public class Obrero {
    private String rut;
    private int edad;

    public Obrero(String rut, int edad) {
        this.rut = rut;
        this.edad = edad;
    }

    public String getRut() {
        return rut;
    }

    public int getEdad() {
        return edad;
    }
}
