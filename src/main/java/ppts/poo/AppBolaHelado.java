
package ppts.poo;

public class AppBolaHelado {
    public static void main(String[] args) {
        BolaHelado bola1 = new BolaHelado("azul","Chirimoya");
        BolaHelado bola2 = new BolaHelado("Naranjo","Lúcuma");
        BolaHelado bola3 = new BolaHelado("Rosado","Frutilla");
        
        System.out.println(bola1.comer());
        System.out.println(bola2.comer());
        System.out.println(bola3.comer());
    }
}
