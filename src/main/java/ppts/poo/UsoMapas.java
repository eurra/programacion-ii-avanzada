
package ppts.poo;

import java.util.HashMap;
import java.util.Iterator;

public class UsoMapas {
    public static void main(String[] args) {
        // Crear mapa
        HashMap<String, CuentaCorriente> cuentas = new HashMap<>();
        
        // Agregar / reemplazar elementos   
        cuentas.put("rut1", new CuentaCorriente("rut1"));
        
        CuentaCorriente c = new CuentaCorriente("rut2");
        cuentas.put("rut2", c);
        
        cuentas.put("rut3", new CuentaCorriente("rut3"));
        cuentas.put("rut4", new CuentaCorriente("rut4"));
        
        // Obtener elemento
        System.out.println(cuentas.get("rut1").getSaldo());
        
        // Iterar elementos
        Iterator<String> keys = cuentas.keySet().iterator();
        
        while(keys.hasNext()) {
            CuentaCorriente cuenta = cuentas.get(keys.next());
            System.out.println("Rut " + cuenta.getRut());
        }
        
        // Eliminar elementos
        cuentas.remove("rut");
        
        // Obtener cantidad de elementos
        System.out.println("Tamaño: " + cuentas.size());
                
        // Borrar todos los elementos
        cuentas.clear();
    }
}
