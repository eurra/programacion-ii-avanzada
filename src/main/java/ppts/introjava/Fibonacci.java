
package ppts.introjava;

public class Fibonacci {
    public static void main(String[] args) {
        int n1 = 0;
        int n2 = 1;
        int curr = 2;
        
        while(curr <= 40) {
            int fib = n1 + n2;            
            n1 = n2;
            n2 = fib;
            
            curr++;
        }
        
        System.out.println("f(40) = " + n2);
    }
}
