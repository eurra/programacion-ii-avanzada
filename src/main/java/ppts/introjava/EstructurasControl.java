
package ppts.introjava;

public class EstructurasControl {
    public static void main(String[] args) {
        int numTablas = 12;
        int maxPorTabla = 12;
        
        int i = 1;        
        
        while(i <= numTablas) {
            System.out.println("Mostrando tabla del " + i + ":");
            int j = 1;
            
            while(j <= maxPorTabla) {
                int mult = i * j;
                System.out.println(i + " x " + j + " = " + mult);                
                j++;
            }
            
            i++;
            System.out.println();
        }
    }
}
