
package ppts.introjava;

import java.util.Scanner;

public class PromedioNotas4 {
    /**
     * Retorna la posición (índice) de un alumno buscado en el arreglo de RUTs,
     * o -1 si no se encuentra.
     * @param ruts Arreglo de ruts
     * @param rutBuscado Rut del alumno buscado
     * @return el índice en donde se encuentra el alumno, o -1 si es que no se 
     *  encuentra.
     */
    private static int buscarAlumno(String[] ruts, String rutBuscado) {
        for(int i = 0; i < ruts.length; i++) {
            if(ruts[i] != null && ruts[i].equals(rutBuscado))
                return i;
        }
        
        return -1;
    }
    
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        
        System.out.println("Ingrese cantidad de notas a calcular");        
        int cantNotas = lector.nextInt();
        
        System.out.println("Ingrese cantidad de alumnos");
        int cantAlumnos = lector.nextInt();
        
        String ruts[] = new String[cantAlumnos];
        double notas[][] = new double[cantAlumnos][cantNotas];
        boolean continuar = true;
        
        do {
            System.out.println("Seleccione opción:");
            System.out.println("1 - Agregar nuevo alumno");
            System.out.println("2 - Agregar una nota a un alumno");
            System.out.println("3 - Eliminar alumno");
            System.out.println("4 - Calcular el promedio de notas de un alumno");
            System.out.println("5 - Calcular el promedio de notas de una evaluación");
            System.out.println("6 - Salir");
            
            int opcion = lector.nextInt();
            
            switch(opcion) {
                case 1: {
                    System.out.println("Ingresar RUT a agregar");
                    String nuevoRUT = lector.next();
                                      
                    int indiceAlumno = buscarAlumno(ruts, nuevoRUT);
                    
                    if(indiceAlumno != -1) {
                        System.out.println("El RUT ingresado ya existe");
                    } 
                    else {
                        int newPos = -1;
                        
                        for(int i = 0; i < ruts.length && newPos == -1; i++) {
                            if(ruts[i] == null)
                                newPos = i;
                        }
                        
                        if(newPos == -1) {
                            System.out.println("No hay espacio para agregar al alumno");
                        }
                        else {
                            ruts[newPos] = nuevoRUT;
                            
                            for(int i = 0; i < cantNotas; i++)
                                notas[newPos][i] = -1.0;
                            
                            System.out.println("Alumno agregado exitosamente");
                        }
                    }
                    
                    break;
                }
                case 2: {
                    System.out.println("Ingresar RUT al cual se agregará la evaluación");
                    String nuevoRUT = lector.next();
                                      
                    int indiceAlumno = buscarAlumno(ruts, nuevoRUT);
                    
                    if(indiceAlumno == -1) {
                        System.out.println("El RUT ingresado NO existe");
                    } 
                    else {
                        System.out.println("Ingresar evaluación a asignar");
                        int eval = lector.nextInt();                        
                        
                        System.out.println("Ingrese nota a registrar");
                        double nota = lector.nextDouble();
                        
                        // Se asume que el número de evaluación es correcto
                        notas[indiceAlumno][eval] = nota;
                        System.out.println("Nota registrada correctamente!");
                    }
                    
                    break;
                }
                case 3: {
                    System.out.println("Ingresar RUT a eliminar");
                    String nuevoRUT = lector.next();
                                      
                    int indiceAlumno = buscarAlumno(ruts, nuevoRUT);
                    
                    if(indiceAlumno == -1) {
                        System.out.println("El RUT ingresado NO existe");
                    } 
                    else {
                        ruts[indiceAlumno] = null;
                        
                        for(int i = 0; i < notas[indiceAlumno].length; i++)
                            notas[indiceAlumno][i] = -1.0;
                        
                        System.out.println("Alumno eliminado exitosamente!");
                    }
                    
                    break;
                }
                case 4: {
                    System.out.println("Ingresar RUT para calculo de promedio");
                    String nuevoRUT = lector.next();
                                      
                    int indiceAlumno = buscarAlumno(ruts, nuevoRUT);
                    
                    if(indiceAlumno == -1) {
                        System.out.println("El RUT ingresado NO existe");
                    } 
                    else {
                        int totalNotas = 0;
                        double sumaNotas = 0.0;
                        
                        for(int i = 0; i < notas[indiceAlumno].length; i++) {
                            if(notas[indiceAlumno][i] != -1) {
                                totalNotas++;
                                sumaNotas += notas[indiceAlumno][i];
                            }
                        }
                        
                        if(totalNotas == 0)
                            System.out.println("El alumno no registraba notas ingresadas");
                        else
                            System.out.println("El promedio de notas del alumno es " + (sumaNotas / totalNotas));
                    }
                    
                    break;
                }
                case 5: {
                    System.out.println("Ingresar número de evaluación para la que se calculará el promedio");
                    int eval = lector.nextInt();
                    int totalNotas = 0;
                    double sumaNotas = 0.0;
                    
                    for(int i = 0; i < notas.length; i++) {
                        if(notas[i][eval] != -1.0) {
                            totalNotas++;
                            sumaNotas += notas[i][eval];
                        }
                    }
                    
                    if(totalNotas == 0)
                        System.out.println("No existen notas registradas en la evaluación");
                    else
                        System.out.println("El promedio de notas de la evaluación es " + (sumaNotas / totalNotas));
                }
                case 6: {
                    continuar = false;
                }
            }
        }
        while(continuar);      
    }
}
