
package ppts.introjava;

public class EstructurasControl2 {
    public static void main(String[] args) {
        int numTablas = 12;
        int maxPorTabla = 12;
        
        int i = 1;
        int sumaMenores20 = 0;
        int sumaMayores50 = 0;
        int sumaOtros = 0;
        
        while(i <= numTablas) {
            System.out.println("Mostrando tabla del " + i + ":");
            int j = 1;
            
            while(j <= maxPorTabla) {
                int mult = i * j;
                
                if(mult < 20)
                    sumaMenores20 += mult;
                else if(mult > 50)
                    sumaMayores50 += mult;
                else
                    sumaOtros += mult;
                
                System.out.println(i + " x " + j + " = " + mult);                
                j++;
            }
            
            i++;
            System.out.println();
        }
        
        System.out.println("La suma de las multiplicaciones menores a 20 es: " + sumaMenores20);
        System.out.println("La suma de las multiplicaciones mayores a 50 es: " + sumaMayores50);
        System.out.println("La suma de las multiplicaciones del resto de los números es: " + sumaOtros);
    }
}
