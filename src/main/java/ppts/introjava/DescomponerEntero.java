
package ppts.introjava;

import java.util.Scanner;

public class DescomponerEntero {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        System.out.println("Ingrese número entero a descomponer");
        int num = lector.nextInt();
        int copiaNum = num;
        
        do {
            int digito = copiaNum % 10;
            System.out.println("Siguiente dígito: " + digito);
            copiaNum = copiaNum / 10;
        }
        while(copiaNum != 0);
    }
}
