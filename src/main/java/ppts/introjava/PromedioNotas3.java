
package ppts.introjava;

import java.util.Scanner;

public class PromedioNotas3 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        
        System.out.println("Ingrese cantidad de notas a calcular");        
        int cantNotas = lector.nextInt();
        
        System.out.println("Ingrese cantidad de alumnos");
        int cantAlumnos = lector.nextInt();
        
        double notas[][] = new double[cantAlumnos][cantNotas];
        int i = 0;
        
        while(i < cantAlumnos) {
            int j = 0;
            System.out.println("Ingresando notas del alumno " + (i + 1));
            
            while(j < cantNotas) {
                System.out.println("Ingrese próxima nota: ");
                notas[i][j] = lector.nextDouble();
                
                j++;
            }
            
            i++;
        }
        
        i = 0;        
        double sumaTotal = 0;
        
        while(i < cantAlumnos) {
            int j = 0;
            double sumaAlumno = 0;
            
            while(j < cantNotas) {
                sumaTotal += notas[i][j];
                sumaAlumno += notas[i][j];
                
                j++;
            }
            
            double promAlumno = sumaAlumno / cantNotas;
            System.out.println("El promedio de las notas del alumno " + (i + 1) + " es: " + promAlumno);
            
            i++;
        }
        
        double promTotal = sumaTotal / (cantNotas * cantAlumnos);
        System.out.println("El promedio de las notas del todos los alumnos es: " + promTotal);        
    }
}
