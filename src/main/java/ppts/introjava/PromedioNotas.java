
package ppts.introjava;

import java.util.Scanner;

public class PromedioNotas {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        double suma = 0;
        int cant = 0;
        boolean continuar = true;
        
        do {
            System.out.println("Ingrese próxima nota: ");
            double nota = lector.nextDouble();
            
            suma += nota;
            cant++;
            
            System.out.println("Desea continuar? (S/N)");
            String opcion = lector.next();
            
            if(opcion.equalsIgnoreCase("S") == false)
                continuar = false;
        }
        while(continuar == true);
        
        double prom = suma / cant;
        System.out.println("El promedio de las notas ingresadas es: " + prom);
    }
}
