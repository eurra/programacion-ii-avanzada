
package ppts.introjava;

public class VariablesTipos {
    public static void main(String[] args) {
        int numEntero = 10;
        double PI = 3.14159265359;
        
        String cadenaTexto = "Compadre compreme un coco, compadre coco no compro, "
            + "el que poco coco come, poco coco compra, "
            + "yo como poco coco como, poco coco compro.";
        
        boolean varLogica = true;
        
        System.out.println("Imprimiendo número entero: " + numEntero);
        System.out.println("Imprimiendo número decimal: " + PI);
        System.out.println("Imprimiendo cadena de texto: \"" + cadenaTexto + "\"");
        System.out.println("Imprimiendo boolean (valor lógico): " + varLogica);
        
        double decimalPromAuto = numEntero;
        int enteroCasting = (int)PI;
        
        System.out.println("Imprimiendo número decimal convertido por promoción automática: " + decimalPromAuto);
        System.out.println("Imprimiendo número entero convertido por casting: " + enteroCasting);
        
        // si descomenta la siguiente linea.. ¿Por qué tiene error?
        // int otroNumero = PI;
    }
}
