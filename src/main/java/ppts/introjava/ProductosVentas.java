
package ppts.introjava;

import java.util.Scanner;

public class ProductosVentas {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        System.out.println("Ingrese cantidad de productos:");        
        String[] nombres = new String[lector.nextInt()];
        int[] ventas = new int[nombres.length];
        boolean continuar = true;

        do {
            System.out.println("Seleccione una opción:");
            System.out.println("1 - Agregar un producto nuevo");
            System.out.println("2 - Sumar ventas a un producto");
            System.out.println("3 - Calcular promedio de ventas");
            System.out.println("4 - Salir");
            int opc = lector.nextInt();

            switch(opc) {
                case 1: {
                    System.out.println("Ingrese nombre de producto a agregar");
                    String nombre = lector.next();

                    // Buscar repetido
                    boolean repetido = false;

                    for(int i = 0; i < nombres.length && !repetido; i++) {
                        if(nombres[i] != null && nombres[i].equals(nombre))
                            repetido = true;
                    }

                    if(repetido) {
                        System.out.println("El nombre especificado ya existe");
                    }
                    else {
                        // Buscar posición disponible
                        int pos = -1;

                        for(int i = 0; i < nombres.length && pos == -1; i++) {
                            if(nombres[i] == null)
                                pos = i;
                        }

                        if(pos == -1) {
                            System.out.println("No hay capacidad para agregar otro producto");
                        }                            
                        else {
                            nombres[pos] = nombre;
                            System.out.println("Producto agregado exitosamente!");
                        }
                    }

                    break;
                }
                case 2: {
                    System.out.println("Ingrese nombre de producto al que se sumaran ventas");
                    String nombre = lector.next();
                    
                    // Buscar producto
                    int pos = -1;

                    for(int i = 0; i < nombres.length && pos == -1; i++) {
                        if(nombres[i] != null && nombres[i].equals(nombre))
                            pos = i;
                    }

                    if(pos == -1) {
                        System.out.println("Producto no existe");
                    }                            
                    else {
                        System.out.println("Ingrese monto de ventas a sumar");
                        int cantVentas = lector.nextInt();
                        
                        ventas[pos] += cantVentas;
                        System.out.println("Ventas sumadas exitosamente!");
                    }
                    
                    break;
                }
                case 3: {
                    double suma = 0.0;
                    int cant = 0;
                    
                    for(int i = 0; i < nombres.length; i++) {
                        if(nombres[i] != null) {
                            suma += ventas[i];
                            cant++;
                        }
                    }
                    
                    if(cant == 0)
                        System.out.println("No se registran productos para calcular el promedio");
                    else
                        System.out.println("El promedio de ventas de los productos es " + (suma / cant));
                    
                    break;
                }
                case 4: {
                    continuar = false;
                }
            }
        }
        while(continuar);
    }
}
