
package ppts.introjava;

import java.util.Scanner;

public class PromedioNotas2 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        
        System.out.println("Ingrese cantidad de notas a calcular");        
        int cant = lector.nextInt();
        double notas[] = new double[cant];
        int i = 0;
        
        while(i < cant) {
            System.out.println("Ingrese próxima nota: ");
            notas[i] = lector.nextDouble();
            
            i++;
        }
        
        i = 0;
        double suma = 0;
        
        while(i < cant) {
            suma += notas[i];
            i++;
        }
        
        double prom = suma / cant;
        System.out.println("El promedio de las notas ingresadas es: " + prom);
    }
}
