
package ppts.introjava;

import java.util.Scanner;

public class Fibonacci2 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        
        System.out.println("Ingrese fibonacci a calcular:");
        int numero = lector.nextInt();
        
        int n1 = 0;
        int n2 = 1;
        int curr = 2;
        
        while(curr <= numero) {
            int fib = n1 + n2;            
            n1 = n2;
            n2 = fib;
            
            curr++;
        }
        
        System.out.println("f(" + numero + ") = " + n2);
    }
}
